import { StatusIndicatorPage } from './app.po';

describe('status-indicator App', () => {
  let page: StatusIndicatorPage;

  beforeEach(() => {
    page = new StatusIndicatorPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
